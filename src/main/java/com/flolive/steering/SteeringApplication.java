package com.flolive.steering;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class SteeringApplication {

	public static void main(String[] args) {
		SpringApplication.run(SteeringApplication.class, args);
	}
}
