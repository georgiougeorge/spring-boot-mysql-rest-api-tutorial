package com.flolive.steering.repository;

import com.flolive.steering.model.Steering;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SteeringRepository extends JpaRepository<Steering, Long> {

}
