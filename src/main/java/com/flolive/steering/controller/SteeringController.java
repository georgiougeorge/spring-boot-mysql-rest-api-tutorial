package com.flolive.steering.controller;

import com.flolive.steering.exception.ResourceNotFoundException;
import com.flolive.steering.model.Steering;
import com.flolive.steering.repository.SteeringRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class SteeringController {

    @Autowired
    SteeringRepository steeringRepository;

    @GetMapping("/steering")
    public List<Steering> getAllSteerings() {
        return steeringRepository.findAll();
    }

    @PostMapping("/steerings")
    public Steering createSteering(@Valid @RequestBody Steering steering) {
        return steeringRepository.save(steering);
    }

    @GetMapping("/steerings/{id}")
    public Steering getSteeringById(@PathVariable(value = "id") Long steeringId) {
        return steeringRepository.findById(steeringId)
                .orElseThrow(() -> new ResourceNotFoundException("Steering", "id", steeringId));
    }

    @PutMapping("/steerings/{id}")
    public Steering updateSteering(@PathVariable(value = "id") Long steeringId,
                                           @Valid @RequestBody Steering steeringDetails) {

        Steering steering = steeringRepository.findById(steeringId)
                .orElseThrow(() -> new ResourceNotFoundException("Steering", "id", steeringId));

        steering.setName(steeringDetails.getName());
        steering.setMcc_mnc(steeringDetails.getMcc_mnc());

        Steering updatedSteering = steeringRepository.save(steering);
        return updatedSteering;
    }

    @DeleteMapping("/steerings/{id}")
    public ResponseEntity<?> deleteSteering(@PathVariable(value = "id") Long steeringId) {
        Steering steering = steeringRepository.findById(steeringId)
                .orElseThrow(() -> new ResourceNotFoundException("Steering", "id", steeringId));

        steeringRepository.delete(steering);

        return ResponseEntity.ok().build();
    }
}
